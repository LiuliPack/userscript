// ==UserScript==
// @name               萌百_去黑幕
// @name:zh-CN         萌百_去黑幕
// @name:en-US         MoeGirl_Remove mask
// @description        去除萌娘百科的黑幕。
// @version            1.0.4
// @author             LiuliPack
// @license            WTFPL
// @namespace          https://gitlab.com/LiuliPack/UserScript
// @include            /(m)?zh.moegirl.org.cn/
// @grant              GM_addStyle
// @updateURL          https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/MoeGirl_Remove%20mask.user.js
// @downloadURL        https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/MoeGirl_Remove%20mask.user.js
// @run-at             document-end
// ==/UserScript==

'use strict';

// 修改样式
GM_addStyle('.heimu{color:#fff!important}.heimu a,.mw-body a.external:visited{color:#5b84c8!important}');

// 去除`title`属性。
document.querySelectorAll('.heimu').forEach(ele => {
    ele.removeAttribute('title');
});