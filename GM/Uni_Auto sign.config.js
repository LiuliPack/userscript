// 自定义脚本中的注释必须采用多行注释，否则压缩后就会把注释后方配置全注释掉
let config = {
    "sign": [
        {
            "remark": "通用(Anime 字幕论坛、NeoACG) - Discuz DSU 每日签到插件",
            "link": /dsu_paulsign-sign.html$|plugin.php?id=dsu_paulsign:sign$/,
            "close": true,
            "check": {
                "mode": "ele",
                "ele": "#yl"
            },
            "sign": [
                {
                    "remark": "选择心情",
                    "mode": "clicker",
                    "ele": "#yl",
                    "delay": 0
                },
                {
                    "remark": "勾选不填写留言",
                    "mode": "clicker",
                    "ele": "#qiandao > table.tfm > tbody > tr:nth-child(1) > td > label:nth-child(3) > input[type=radio]",
                    "delay": 0
                },
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": "#shuai_menu + table .tac a",
                    "delay": 0
                },
                {
                    "remark": "Anime 字幕论坛专有 - 浏览「Discuz 任务」页",
                    "mode": "script",
                    "data": () => {
                        /*如果页面匹配就打开「Discuz 任务」页*/
                        if(URL.search(/bbs.acgrip.com/) !== -1) {
                            $('#m_menu li:nth-child(9) a').click();
                        }
                    },
                    "delay": 2500
                }
            ]
        },
        {
            "remark": "萌幻之乡、芯幻 - WordPress INN AO 主题",
            "link": /https:\/\/(www.hmoeh.com|xhcyra.com)\/author\/\d+/,
            "close": true,
            "check": {
                "mode": "attr",
                "ele": "#inn-nav__point-sign-daily a",
                "data": {
                    "key": "title",
                    "value": "签到"
                }
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": "#inn-nav__point-sign-daily a",
                    "delay": 0
                }
            ]
        },
        {
            "remark": "Anime 字幕论坛 - Discuz 任务",
            "link": "https://bbs.acgrip.com/home.php?mod=task",
            "close": true,
            "check": {
                "mode": "ele",
                "ele": "#ct a[href='home.php?mod=task&do=apply&id=1']"
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": "#ct a[href='home.php?mod=task&do=apply&id=1']",
                    "delay": 0
                }
            ]
        },
        {
            "remark": "Anime 字幕论坛 - Discuz 任务完成",
            "link": "https://bbs.acgrip.com/home.php?mod=task&item=done",
            "close": true,
            "check": {
                "mode": "ele",
                "ele": "#mn_N462e a"
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": "#mn_N462e a",
                    "delay": 0
                }
            ]
        },
        {
            "remark": "2DFun",
            "link": /https:\/\/2dfan.com\/(users\/\d+\/recheckin)?$/,
            "close": true,
            "check": {
                "mode": "ele",
                "ele": "#do_checkin"
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": "#do_checkin"
                }
            ]
        },
        {
            "remark": "南+ - 申请任务",
            "link": "https://www.south-plus.net/plugin.php?H_name-tasks.html.html",
            "close": true,
            "check": {
                "mode": "ele",
                "ele": "#p_15 a"
            },
            "sign": [
                {
                    "remark": "申请任务「日常」",
                    "mode": "clicker",
                    "ele": "#p_15 a",
                    "delay": 0
                },
                {
                    "remark": "申请任务「周常」",
                    "mode": "clicker",
                    "ele": "#p_14 a",
                    "delay": 0
                },
                {
                    "remark": "前往「完成任务」页",
                    "mode": "clicker",
                    "ele": "tr.tr3:nth-child(3) td a",
                    "delay": 2000
                }
            ]
        },
        {
            "remark": "南+ - 完成任务",
            "link": "https://www.south-plus.net/plugin.php?H_name-tasks-actions-newtasks.html.html",
            "close": true,
            "check": {
                "mode": "ele",
                "ele": "#both_15 a"
            },
            "sign": [
                {
                    "remark": "完成任务「日常」",
                    "mode": "clicker",
                    "ele": "#both_15 a",
                    "delay": 0
                },
                {
                    "remark": "完成任务「周常」",
                    "mode": "clicker",
                    "ele": "#both_14 a",
                    "delay": 0
                }
            ]
        },
        {
            "remark": "绯月",
            "link": "https://bbs.kfpromax.com/kf_growup.php",
            "close": true,
            "check": {
                "mode": "ele",
                "ele": ".gro_divhui:nth-child(5) + div a:not(a[href='javascript:;'])"
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": ".gro_divhui:nth-child(5) + div a",
                    "delay": 0
                }
            ]
        },
        {
            "remark": "绅士仓库",
            "link": "https://cangku.moe/",
            "close": true,
            "check": {
                "mode": "text",
                "ele": ".auth-info .footer li:nth-child(2) a",
                "data": "签到"
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": ".auth-info .footer li:nth-child(2) a",
                    "delay": 0
                }
            ]
        },
        {
            "remark": "紳士の庭",
            "link": "https://gmgard.moe/",
            "close": true,
            "check": {
                "mode": "text",
                "ele": "#checkw",
                "data": "点此签到"
            },
            "sign": [
                {
                    "remark": "签到执行",
                    "mode": "clicker",
                    "ele": "#checkw",
                    "delay": 2000
                }
            ]
        }
    ],
    "open": [
        {
            "remark": "Anime 字幕论坛 - Discuz 任务",
            "enable": false,
            "link": "https://bbs.acgrip.com/home.php?mod=task"
        },
        {
            "remark": "NeoACG",
            "enable": false,
            "link": "https://neoacg.com/dsu_paulsign-sign.html"
        },
        {
            "remark": "萌幻之乡",
            "enable": false,
            "link": "https://www.hmoeh.com/author/143569"
        },
        {
            "remark": "芯幻",
            "enable": false,
            "link": "https://xhcyra.com/author/1000001"
        },
        {
            "remark": "2DFun",
            "enable": false,
            "link": "https://2dfan.com/"
        },
        {
            "remark": "南+",
            "enable": false,
            "link": "https://www.south-plus.net/plugin.php?H_name-tasks.html.html"
        },
        {
            "remark": "绯月",
            "enable": false,
            "link": "https://bbs.kfpromax.com/kf_growup.php"
        },
        {
            "remark": "绅士仓库",
            "enable": false,
            "link": "https://cangku.moe/"
        },
        {
            "remark": "紳士の庭",
            "enable": false,
            "link": "https://gmgard.moe/"
        },
    ]
}