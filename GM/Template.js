// ==UserScript==
// @name               站点名_功能
// @name:zh-CN         站点名_功能
// @name:en-US         SiteName_Func
// @description        简介
// @version            1.0.0
// @author             LiuliPack
// @license            WTFPL
// @namespace          https://gitlab.com/LiuliPack/UserScript
// @antifeature        ads           展示广告
// @antifeature        membership    订阅内容后可用
// @antifeature        miner         计算机挖矿
// @antifeature        payment       支付钱款后可用
// @antifeature        referral-link 含有返利
// @antifeature        tracking      分析使用状况
// @compatible         firefox       火狐上必须关闭广告过滤器
// @incompatible       chrome        酷容
// @incompatible       opera         欧朋
// @incompatible       safari        Apple Safari
// @incompatible       edge          MS Edge
// @match              https://*/*
// @exclude            http://*/*
// @connect            none
// @contributionURL    https://b23.tv/BV1ME411r78W
// @contributionAmount 0
// @icon               data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==
// @icon64             data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==
// @resource           html https://www.tampermonkey.net/index.html
// @require            none
// @grant              none
// @updateURL          https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/
// @downloadURL        https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/
// @run-at             document-end
// @noframes
// @unwrap
// ==/UserScript==

'use strict';

/*(function() {
    'use strict';

    // Your code here...
})();*/