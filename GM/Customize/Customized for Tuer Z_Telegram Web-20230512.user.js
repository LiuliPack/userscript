// ==UserScript==
// @name               Tuer Z 定制_网页 Telegram-20230512
// @name:zh-CN         Tuer Z 定制_网页 Telegram-20230512
// @name:en-US         Customized for Tuer Z_Telegram Web-20230512
// @description        每两秒检测返回最新消息按钮是否存在且被展示，如果存在就点击。2023年5月12日在 https://greasyfork.org/zh-CN/discussions/requests/183033 为 Tuer Z 定制的脚本。
// @version            1.1.0
// @author             LiuliPack
// @license            WTFPL
// @namespace          https://gitlab.com/LiuliPack/UserScript
// @match              https://web.telegram.org/k/
// @updateURL          https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Customize/Customized%20for%20Tuer%20Z_Telegram%20Web-20230512.user.js
// @downloadURL        https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Customize/Customized%20for%20Tuer%20Z_Telegram%20Web-20230512.user.js
// @run-at             document-end
// ==/UserScript==

'use strict';

// 每两秒循环一次
setInterval(() => {
    // 定义返回最新消息按钮(btn)变量
    let btn = document.querySelector('.chat-input .bubbles-go-down:not(.chat-input.is-hidden .bubbles-go-down)');

    // 如果按钮存在就点击它
    if( btn ) { btn.click(); }
}, 2000);