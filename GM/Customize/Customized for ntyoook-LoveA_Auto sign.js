// ==UserScript==
// @name               ntyoook 定制-LoveA_自动签到
// @name:zh-CN         ntyoook 定制-LoveA_自动签到
// @name:en-US         Customized for ntyoook-LoveA_Auto sign
// @description        为 ntyoook 定制得 LoveA 自动签到脚本。
// @version            1.0.0
// @author             LiuliPack
// @license            WTFPL
// @namespace          https://gitlab.com/LiuliPack/UserScript
// @match              https://www.lovea.fun/user
// @supportURL         https://gitlab.com/liulipack/UserScript/issues
// ==/UserScript==

'use strict';

// 定义签到按钮(btn)变量。
let btn = document.querySelector('.checkin');

// 如果未签到。
if(btn.textContent === "今日签到") {

    // 等待 0-3 秒，模拟真实点击。
    setTimeout(() => {

        // 签到执行！
        btn.click()

    }, Math.random() * 3 * 1000);
}