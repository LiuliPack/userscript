// ==UserScript==
// @name               DEXTools_自动点击更多信息按钮
// @name:zh-CN         DEXTools_自动点击更多信息按钮
// @name:en-US         DEXTools_Auto click more info btn
// @description        在适当的时候自动点击更多信息按钮。
// @version            1.0.3-hotfix
// @author             LiuliPack
// @license            WTFPL
// @namespace          https://gitlab.com/LiuliPack/UserScript
// @match              https://www.dextools.io/app/en/ether/pair-explorer/*
// @match              https://www.dextools.io/app/es/ether/pair-explorer/*
// @match              https://www.dextools.io/app/cn/ether/pair-explorer/*
// @match              https://www.dextools.io/app/es/ether/pair-explorer/*
// @match              https://www.dextools.io/app/ru/ether/pair-explorer/*
// @match              https://www.dextools.io/app/ar/ether/pair-explorer/*
// @match              https://www.dextools.io/app/hi/ether/pair-explorer/*
// @run-at             document-body
// ==/UserScript==

'use strict';

// 定义快捷选素选择器($)变量，元素快捷选取($(元素定位符))函数
let $ = ele => document.querySelector(ele);

function $$(ele) {
    return new Promise(resolve => {
        if ($(ele)) {
            return resolve($(ele));
        }
        const observer = new MutationObserver(mutations => {
            if ($(ele)) {
                resolve($(ele));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
};

// 当趋势信息被展示、左侧信息栏未展开时，就点击更多信息按钮
$$('.trading-view__container').then(ele => {
    if($('.left-container > div .mt-0').textContent === ' More info ') {
        $('.section-container .more-info-button').click();
    }
});