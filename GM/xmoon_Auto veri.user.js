// ==UserScript==
// @name               星月号_网盘自动验证
// @name:zh-CN         星月号_网盘自动验证
// @name:en-US         xmoon_Auto veri
// @description        自动填充星月号附属网盘提取码并提交。
// @version            1.0.1
// @author             LiuliPack
// @license            WTFPL
// @namespace          https://gitlab.com/LiuliPack/UserScript
// @include            /(box|pan)\.1024\.(wtf|rip|ski|vet|fyi)\/\?dl=*/
// @updateURL          https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/xmoon_Auto%20veri.user.js
// @downloadURL        https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/xmoon_Auto%20veri.user.js
// @run-at             document-body
// ==/UserScript==

'use strict';

// 定义元素存在函数($$(元素定位符))
let $ = ele => document.querySelector(ele);

// 填充密码并提交
$('input[type="password"]').value = 'xmoon';
$('button[type="submit"]').click();