# 关于 / About
油猴、AutoHotKey 脚本。采用 [WTFPL](LICENSE) 授权。  
Greasemonkey & AutoHotKey UserScript. Licensed using [WTFPL](LICENSE).


# 清单 / Lists

## Greasemonkey

面向网站(Title) | 描述(Desc) | 安装(Inst)
| - | - | -
通用_网页链接修改(Uni_URI Modify) | 自动替换对应域名的关键内容。此脚本受 https://greasyfork.org/zh-CN/scripts/2312 启发。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_URI%20Modify.user.js)
B站、西瓜_制裁低价值号(BILI, ixigua_Kick Low-value creators) | 检测到到低创内容作者后阻止播放行为，降低完播率。从而降低平台收益。目前支持哔哩哔哩和西瓜视频(字节跳动旗下中长视频平台)。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/BILI,%20ixigua_Kick%20Low-value%20creators.user.js)
通用_加载更多(Uni_More Message) | 通过 Shift 加左右方向键实现快速页面切换，配置为加载更多模式还可全自动操作。它前身是受到 [woyaokaiche](https://www.hacg.me/wp/bbs/postid/42751) 请求而写，现在它支持更多网站。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_More%20Message.user.js)
通用_反超链接拦截(Uni_Anti URL-Blocker) | 自动完成超链接跳转。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_Anti%20URL-Blocker.user.js)
通用_次要页面音视频处理(Uni_Secondary pages multimedia processer) | 非暂停或静音非聚焦页面。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_Secondary%20pages%20multimedia%20processer.user.js)
B站_播放器自动化(Bili_Player Automation) | 播放时自动点击网页全屏、播放完成后自动退出全屏。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/BILI_Player%20Automation.user.js)
琉璃神社_伪站判断(HACG_Phishing detect) | 通过检测网站标题、域名和特征元素特征判断站点真伪。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/HACG_Phishing%20detect.user.js)
星月号_网盘自动验证(xmoon_Auto veri) | 自动填充星月号附属网盘提取码并提交。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/xmoon_Auto%20veri.user.js)
通用_自动签到(Uni_Auto sign) | 全或半自动签到。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_Auto%20sign.user.js)
通用_敏感内容屏蔽(Uni_Porn blocker) | 通过关键词匹配域名、网站标题和网站内容，阻止访问色情网站。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_Porn%20blocker.user.js)
萌百_去黑幕(MoeGirl_Remove mask) | 去除萌娘百科和 H 萌娘的黑幕。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/MoeGirl_Remove%20mask.user.js)
琉璃神社_伪大佬(HACG_Fake ace) | 替换神社等级图标。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/HACG_Fake%20ace.user.js)
通用_开放注册检测(Uni_Allow reg checker) | 通过比对元素文本判断，检测开放注册状态。 | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/GM/Uni_Allow%20reg%20checker.user.js)

## AutoHotKey

描述(Desc) | 获取(Get)
| - | -
鼠标连点器。(Mouse clicker.) | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/AHK/Clicker/Clicker_latest.ahk)

## HeaderEditor

描述(Desc) | 获取(Get)
| - | -
\`通用_网页链接修改\`附属(\`Uni_URI Modify\`Affiliate) | [获取/Get](https://gitlab.com/LiuliPack/userscript/-/raw/main/HE/\`Uni_URI%20Modify`Affiliate.json)

# 附录 / Appendix

## 约定式提交 1.0.6-本地化 / conventional commits 1.0.6-localization

本仓库基于[约定式提交 1.0.0](https://www.conventionalcommits.org/zh-hans/v1.0.0/约定式提交规范)的本地化版本。

```
[提交类型]-[涉及代码类型](涉及代码1, 涉及代码2)：(更新描述1)；(更新描述2)。

(更新列表)

(发布人)

---

[必填] (可选)
```

提交类型 | 描述
-- | -
`新` | 新增功能。
`修` | 修正错误。
`优` | 优化代码。
`重` | 仅重构代码，不修复错误和新增功能。
`文` | 调整文档。
`整` | 调整文件位置。
`撤` | 撤销提交。

涉及代码类型 | 描述
-- | -
`油` | 油猴脚本。
`热` | AutoHotKey 脚本。
`头` | 标头编辑器(HeaderEditor) 规则。